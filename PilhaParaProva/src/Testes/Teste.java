package Testes;

import Codigo.Pilha;

public class Teste {

	public static void main(String[] args) {
		String frase = "amor a roma";
		char[] fraseSeparada = frase.toCharArray();
		System.out.println(palidroma(fraseSeparada));
	}

	private static boolean palidroma(char[] fraseSeparada) {
		boolean resultado = true;
		Pilha<Character> pilha = new Pilha<>(fraseSeparada.length);
		for (int i = 0; i < fraseSeparada.length; i++) {
			pilha.empilhar(fraseSeparada[i]);
		}
		for (int i = 0; i < fraseSeparada.length; i++) {
			if(fraseSeparada[i] != pilha.desempilha()) {
				return false;
			}
		}
		return resultado;
	}
	
}


