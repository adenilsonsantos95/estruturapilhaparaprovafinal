package Codigo;

public class Pilha<T> implements TDAPilha<T> {

	private T[] elementos;
	private int tamanho;
	
	@SuppressWarnings("unchecked")
	public Pilha() {
		this.elementos = (T[]) new Object[10];
		this.tamanho = 0;
		
	}
	@SuppressWarnings("unchecked")
	public Pilha(int capacidade) {
		this.elementos = (T[]) new Object[capacidade];
		this.tamanho = 0;
	}

	private T[] getElementos(){
		return this.elementos;
	}


	@Override
	public void empilhar(T elemento) {
		if(this.estaCheia()) {this.aumentaCapacidade();}
		this.elementos[this.tamanho] = elemento;
		this.tamanho++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void aumentaCapacidade() {
		T[] aux = (T[]) new Object[this.elementos.length * 2];
		for (int i = 0; i < this.elementos.length; i++) {
			aux[i] = this.elementos[i];
		}
		this.elementos = aux;
	}

	@Override
	public boolean estaVazia() {
		return this.tamanho == 0;
	}
	@Override
	public boolean estaCheia() {
		return this.tamanho == this.elementos.length;
	}

	@Override
	public T topo() {
		return this.elementos[this.tamanho - 1];
	}

	@Override
	public T desempilha() {
		if(this.estaVazia()) {return null;}
		return this.elementos[--this.tamanho];
	}

	@Override
	public int tamanho() {
		return this.tamanho;
	}

	public void inverter(){
		Pilha<T> aux = new Pilha<>(10);
		while(!this.estaVazia()){
			aux.empilhar(this.desempilha());
		}
		this.tamanho = aux.tamanho();
		elementos = aux.getElementos();
	}

	@Override
	public String toString() {
		if (this.estaVazia()){ return "Pilha Vazia!";}
		else{
			String print = "Pilha = [";
			int i;
			for (i = 0; i < this.tamanho()-1; i++) {
				print = print + this.elementos[i] + ", ";
			}
			print = print + this.elementos[i] + "] - tamanho = " + this.tamanho();
			return print;
		}

	}

}
