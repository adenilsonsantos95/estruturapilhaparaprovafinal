package Codigo;

public interface TDAPilha<T> {
	public void empilhar(T elemento);
	public void aumentaCapacidade();
	public boolean estaVazia();
	public boolean estaCheia();
	public T topo();
	public T desempilha();
	public int tamanho();
	
}
